################################################################
# lgio.py
#
# Input/Output routines for label graphs.
#
# Author: Richard Zanibbi
# Copyright (c) 2012-2014 Richard Zanibbi and Harold Mouchere
################################################################
import sys
import csv
from lgeval.src.lg import *


def writeDiff(ndiffs, sdiffs, ediffs, streamFile):
    """Write differences for label graphs and (implicit) trees
	to the passed stream."""
    for ndiff in ndiffs:
        streamFile.write("*N," + ndiff[0] + ",")
        for symbol in ndiff[1]:
            streamFile.write(symbol[0] + ",")
            streamFile.write(str(symbol[1]) + ",")
        streamFile.write(":vs:,")

        for i in range(0, len(ndiff[2])):
            streamFile.write(ndiff[2][i][0] + ",")
            streamFile.write(str(ndiff[2][i][1]))
            if i < len(ndiff[2][i]) - 2:
                streamFile.write(",")
        streamFile.write("\n")

    # Edges
    for ediff in ediffs:
        streamFile.write("*E," + ediff[0][0] + "," + ediff[0][1] + ",")
        for symbol in ediff[1]:
            streamFile.write(symbol[0] + ",")
            streamFile.write(str(symbol[1]) + ",")
        streamFile.write(":vs:,")

        for i in range(0, len(ediff[2])):
            streamFile.write(str(ediff[2][i][0]) + ",")
            streamFile.write(str(ediff[2][i][1]))
            if i < len(ediff[2]) - 1:
                streamFile.write(",")
        streamFile.write("\n")

    # Segmentation graph edges. Simply construct all pairs,
    # as disagreeing edges are common.
    for sdiff in list(sdiffs):
        primitiveEndSet = sdiffs[sdiff]
        for end in primitiveEndSet[0]:
            streamFile.write("*S," + str(sdiff) + "," + str(end) + "\n")
        for end in primitiveEndSet[1]:
            streamFile.write("*S," + str(sdiff) + "," + str(end) + "\n")


def writeCSVTuple(tuple, secondComma, streamFile):
    for i in range(0, len(tuple) - 1):
        streamFile.write(str(tuple[i]))
        streamFile.write(",")
    streamFile.write(str(tuple[len(tuple) - 1]))
    if secondComma:
        streamFile.write(",")


def writeMetrics(metrics, streamFile):
    """Computes numerical metrics (for node and edge labels, and segmentation
	graph edge disagreements), and writes them to standard output."""
    numMetrics = len(metrics[0])
    for i in range(0, numMetrics - 1):
        writeCSVTuple(metrics[0][i], True, streamFile)
    writeCSVTuple(metrics[0][numMetrics - 1], False, streamFile)
    streamFile.write("\n")


##################################
# Feb 2024: String generation
#  rather than file writes
##################################
def csv_tuple(tuple_item, secondComma):
    out_str = ''
    for i in range(0, len(tuple_item) - 1):
        out_str += str(tuple_item[i]) + ','
    out_str += str(tuple_item[len(tuple_item) - 1])
    if secondComma:
        out_str += ','
    return out_str

def metric_str(metrics, out_lg, gt_lg):
    """Computes numerical metrics (for node and edge labels, and segmentation
	graph edge disagreements), and creates a string."""

    # File comparison entry ('None' shown for missing output files)
    out_str = '*M,' + out_lg.file + ',' + gt_lg.file + '\n'
    # Line entry for metric values
    num_metrics = len(metrics[0])
    for i in range(0, num_metrics - 1):
        out_str += csv_tuple(metrics[0][i], True)

    out_str += csv_tuple(metrics[0][num_metrics - 1], False) + '\n'

    return out_str


################################################################
# Input
################################################################
def fileListToLgs(fileName):
    """Given a file containing a list of .lg files, return the
	list of corresponding label graphs."""
    file = open(fileName)
    fileReader = csv.reader(file)

    lgList = []
    for row in fileReader:
        # Skip comments and empty lines.
        if not row == [] and not row[0].strip()[0] == "#":
            lgfile1 = row[0].strip()  # remove leading/trailing whitespace
            lg1 = Lg(lgfile1)
            lgList += [lg1]

    file.close()
    return lgList
