################################################################
# evallg.py
#
# Program that reads in two .lg (CSV) files, computes metrics,
# and returns the result as a (CSV) entry, along with a
# CSV entry (row) for each specific error on standard output.
#
# *If run in 'batch' mode, a CSV file for errors and a separate
# file containing all errors observed will be produced.
#
# Author: R. Zanibbi, June 2012
# Copyright (c) 2012-2024 Richard Zanibbi and Harold Mouchere
################################################################
import sys
import csv
import glob, os

from lgeval.src.lg import *
from lgeval.src.lgio import *
import lgeval.src.SmGrConfMatrix as SmGrConfMatrix
import lgeval.src.compareTools as compareTools

# Additions Feb 2024
from lgeval.msg_debug import Debug as db
import lgeval.src.dprl_map_reduce as dprl_mr
from lgeval.src.sum_metric import generate_summary_txt
from lgeval.src.compileLabels import label_str
from multiprocessing import Pool, Value
#import time # debug only

# for RIT web service :
# INKMLVIEWER = "inkml_viewer/index.xhtml?path=../testdata/&files="
# local :
INKMLVIEWER = "http://www.cs.rit.edu/~rlaz/inkml_viewer/index.xhtml?path=http://www.cs.rit.edu/~rlaz/testdata/&files="
MINERRTOSHOW = 3

# HACK: Global variables for map-reduce progress
CURR_COUNT=Value('i',0)
MAX_COUNT=None

def map_lg_file_pair( pair ):
    global CURR_COUNT, MAX_COUNT

    ( out_file, gt_file ) = pair

    # Load files for output and ground truth
    out_lg = Lg( out_file )
    gt_lg = Lg(gt_file)
    metrics = out_lg.compare(gt_lg)
    metric_string = metric_str( metrics, out_lg, gt_lg )

    # Compile node labels while Lg objects available.
    # Attempting to be more compact/efficient than earlier version
    labels = { 'out_node': set(), 'out_edge': set(), 'gt_node': set(), 'gt_edge': set() }

    for odict in out_lg.nlabels.values():
        label = odict.popitem()[0]
        labels['out_node'].add(label)

    for odict in gt_lg.nlabels.values():
        label = odict.popitem()[0]
        labels['gt_node'].add(label)
    
    for odict in out_lg.elabels.values():
        label = odict.popitem()[0]
        labels['out_edge'].add(label)
        
    for odict in gt_lg.elabels.values():
        label = odict.popitem()[0]
        labels['gt_edge'].add(label)

    # HACK: Track progress using global thread-locked variable CURR_COUNT
    with CURR_COUNT.get_lock():
        CURR_COUNT.value += 1
        percentage = CURR_COUNT.value / MAX_COUNT 
        if CURR_COUNT.value < MAX_COUNT:
            print(f'>>> {CURR_COUNT.value} / {MAX_COUNT} files evaluated [ {percentage:3.2%} ]                    \r', end='')
        else:
            print(f'>>> {CURR_COUNT.value} / {MAX_COUNT} files evaluated [ {percentage:3.2%} ]                    \n')
        #time.sleep(0.25) # for debugging

    # Return metrics in a 2-line CSV string and a label dictionary
    return (metric_string, labels)

def reduce_pairs( pair1, pair2):
    # Concatenate metric strings, merge label dictionaries 
    (metrics1, labels1) = pair1
    (metrics2, labels2) = pair2

    out_dict = {}
    for label_set in labels1:
        out_dict[label_set] = labels1[label_set].union(labels2[label_set])

    return (metrics1 + metrics2, out_dict)

def run_batch_parallel(out_dir, gt_dir):
    global CURR_COUNT, MAX_COUNT

    # TO MATCH earlier evaluate script, set match test to label intersection
    compareTools.cmpNodes = compareTools.intersectMetric
    compareTools.cmpEdges = compareTools.intersectMetric
    
    # Running pairs of out/gt files in parallel.

    # Get file lists for output and ground truth files
    cwd = os.getcwd()
    os.chdir(out_dir)
    out_file_list = sorted( glob.glob("*.lg") )
    os.chdir(cwd)

    os.chdir(gt_dir)
    gt_file_list = sorted( glob.glob("*.lg") )
    os.chdir(cwd)

    # NOTE: Assumes non-empty output file list
    file_pairs = []
    missing_files = []
    out_idx = 0
    next_out_file = out_file_list[out_idx]
    for i in range(len(gt_file_list)):
        next_gt_file = gt_file_list[i]

        # If extra output files, skip forward (ignore)
        while next_out_file < next_gt_file:
            out_idx += 1
            if out_idx >= len(out_file_list):
                break
            else:
                next_out_file = out_file_list[out_idx]

        # Create pair if gt/output match found + advance output files
        # DEBUG: Lg class designed to handle output files that are missing
        if next_gt_file == next_out_file:
            file_pairs.append( (os.path.join(out_dir, next_out_file),
            os.path.join(gt_dir, next_gt_file) ))

        else:
            # Record missing files
            file_pairs.append( (os.path.join(out_dir, next_gt_file),
            os.path.join(gt_dir, next_gt_file) ))
            missing_files += [ os.path.join(out_dir, next_gt_file) ]

    # State missing files explicitly
    if len(missing_files) > 0:
        sys.stderr.write('>>> WARNING: ' + str(len(missing_files)) + 
                ' missing output files\n    ' +
               '\n    '.join(missing_files) + '\n\n')


    # Collect metrics for files (map-reduce), and labels
    # HACK: Set global count
    MAX_COUNT=len(file_pairs)
    (metric_str, labels) = dprl_mr.map_reduce(map_lg_file_pair, reduce_pairs, file_pairs)

    # Generate summary and label list texts
    summary_txt = generate_summary_txt(metric_str, out_dir, gt_dir )
    out_label_txt = label_str(labels, 'out_node', 'out_edge')
    gt_label_txt = label_str(labels, 'gt_node', 'gt_edge')

    # Write metrics and summary to LgEval results directory
    eval_out_dir =  os.path.join('Results_' + os.path.basename(out_dir))
    metric_file_path = os.path.join(eval_out_dir, 'RawMetrics.csv')
    summary_file_path = os.path.join(eval_out_dir, 'Summary.txt')
    gt_label_path = os.path.join(eval_out_dir, 'labelsGT.txt')
    out_label_path = os.path.join(eval_out_dir, 'labelsOutput.txt')

    with open(metric_file_path, 'w') as metric_file:
        metric_file.write(metric_str)
    with open(summary_file_path, 'w') as summary_file:
        summary_file.write(summary_txt)
    with open(gt_label_path, 'w') as gt_labels:
        gt_labels.write(gt_label_txt)
    with open(out_label_path, 'w') as out_labels:
        out_labels.write(out_label_txt)

    return summary_txt


def runBatch(fileName, defaultFileOrder, confMat, confMatObj):
    """Compile metrics for pairs of files provided in a CSV
	file. Store metrics and errors in separate files."""
    fileReader = csv.reader(open(fileName))
    metricStream = open(fileName + ".m", "w")
    diffStream = open(fileName + ".diff", "w")

    htmlStream = None
    matrix = None
    matrixObj = None
    if confMat:
        matrix = SmGrConfMatrix.ConfMatrix()
        if confMatObj:
            matrixObj = SmGrConfMatrix.ConfMatrixObject()

    for row in fileReader:
        # Skip comments and empty lines.
        if not row == [] and not row[0].strip()[0] == "#":
            lgfile1 = row[0].strip()  # remove leading/trailing whitespace
            lgfile2 = row[1].strip()
            if not defaultFileOrder:
                temp = lgfile2
                lgfile2 = lgfile1
                lgfile1 = temp
            print("Test: " + lgfile1 + " vs. " + lgfile2)
            toShow = lgfile1
            if len(row) > 2:
                toShow = row[2].strip()
            # Here lg1 is the output, and lg2 the ground truth.
            lg1 = Lg(lgfile1)
            lg2 = Lg(lgfile2)
            out = lg1.compare(lg2)

            metricStream.write("*M," + lgfile1 + "," + lgfile2 + "\n")
            writeMetrics(out, metricStream)
            diffStream.write("DIFF," + lgfile1 + "," + lgfile2 + "\n")
            writeDiff(out[1], out[3], out[2], diffStream)

            nodeClassErr = set()
            edgeErr = set()
            if confMat or confMatObj:
                for (n, _, _) in out[1]:
                    nodeClassErr.add(n)
                for (e, _, _) in out[2]:
                    edgeErr.add(e)

            if confMat:
                for (gt, er) in lg1.compareSubStruct(lg2, [2, 3]):
                    er.rednodes = set(list(er.nodes)) & nodeClassErr
                    er.rededges = set(list(er.edges)) & edgeErr
                    matrix.incr(gt, er, toShow)
            if confMatObj:
                for (obj, gt, er) in lg1.compareSegmentsStruct(lg2, [2]):
                    er.rednodes = set(list(er.nodes)) & nodeClassErr
                    er.rededges = set(list(er.edges)) & edgeErr
                    matrixObj.incr(obj, gt, er, toShow)

        htmlStream = None
    if confMat or confMatObj:
        htmlStream = open(fileName + ".html", "w")
        htmlStream.write('<html xmlns="http://www.w3.org/1999/xhtml">')
        htmlStream.write("<h1> File :" + fileName + "</h1>")
        htmlStream.write(
            "<p>Only errors with at least "
            + str(MINERRTOSHOW)
            + " occurrences appear</p>"
        )
    if confMat:
        htmlStream.write("<h2> Substructure Confusion Matrix </h2>")
        matrix.toHTML(htmlStream, MINERRTOSHOW, INKMLVIEWER)
    if confMatObj:
        htmlStream.write("<h2> Substructure Confusion Matrix at Object level </h2>")
        matrixObj.toHTML(htmlStream, MINERRTOSHOW, INKMLVIEWER)
    if confMat or confMatObj:
        htmlStream.write("</html>")
        htmlStream.close()

    metricStream.close()
    diffStream.close()


def main():
    db.time('imports finished')
    if len(sys.argv) < 3:
        print("Usage: [[python]] evallg.py <file1.lg> <file2.lg> [diff/*]  [INTER]")
        print("   OR  [[python]] evallg.py <file1.lg> <file2.lg> MATRIX fileout")
        print(
            "   OR  [[python]] evallg.py [batch] <filepair_list> [GT-FIRST] [MAT] [MATOBJ] [INTER]"
        )
        print("   OR  [[python]] evallg.py <out_dir> <gt_dir>  --- map-reduce version")
        print("")
        print("    For the first usage, return error metrics and differences")
        print("    for  label graphs in file1.lg and file2.lg.")
        print("    A third argument will return just differences ('diff')")
        print("    or just metrics (any other string). ")
        print("    If MATRIX option is used, 4 evaluations are done with the ")
        print("    different matrix label filters and output in the fileout[ABCD].m]")
        print("")
        print("    For the second usage, a file is provided containing pairs of")
        print("    label graph files, one per line (e.g. 'file1, GTruth').")
        print("    A third optional column contains the file name which should be")
        print("    linked to the InkML viewer.")
        print("")
        print("    A CSV file containing metrics for all comparisons is written")
        print('    to "filepair_list.m", and differences are written to a file')
        print('    "filepair_list.diff". By default ground truth is listed')
        print("    second on each line of the batch file; GT-FIRST as third argument")
        print("    will result in the first element of each line being treated")
        print("    as ground truth - this does not affect metrics (.m), but does")
        print("    affect difference (.diff) output.")
        print("")
        print(
            "    The MAT or MATOBJ option will create a HTML file with confusion Matrix"
        )
        print("    between substructures.")
        print("    MAT will produce the subtructure at stroke level.")
        print("    MATOBJ will produce the subtructure at object level.")
        print("     (in both cases, the size of substructure is 2 or 3 nodes,")
        print("      in both cases, only errors with at least 3 occurrences appear)")
        print("")
        print("[ NEW Feb 2024 ]")
        print("    ** Faster parallelized implementation used if two directories are passed.")
        sys.exit(0)

    showErrors = True
    showMetrics = True

    if "INTER" in sys.argv:
        compareTools.cmpNodes = compareTools.intersectMetric
        compareTools.cmpEdges = compareTools.intersectMetric

    # Feb 2024 -- Faster/simpler map-reduce implementation
    if os.path.isdir(sys.argv[1]):
        out_dir = sys.argv[1].rstrip("/")
        gt_dir = sys.argv[2].rstrip("/")
        summary_txt = run_batch_parallel(out_dir, gt_dir) 
        db.time('evaluation finished')
        db.show_times()
        print('---------------------------------\n\n' + summary_txt)

    elif sys.argv[1] == "batch":
        # If requested, swap arguments.
        defaultFileOrder = True
        confMat = False
        confMatObj = False
        if len(sys.argv) > 3 and "GT-FIRST" in sys.argv:
            print(">> Treating 1st column as ground truth.")
            defaultFileOrder = False
        if len(sys.argv) > 3 and "MAT" in sys.argv:
            print(">> Compute the confusion matrix at primitive level.")
            confMat = True
        if len(sys.argv) > 3 and "MATOBJ" in sys.argv:
            print(">> Compute the confusion matrix at object level.")
            confMatObj = True
        runBatch(sys.argv[2], defaultFileOrder, confMat, confMatObj)

    else:
        # Running for a pair of files: require default order of arguments.
        fileName1 = sys.argv[1]
        fileName2 = sys.argv[2]
        if len(sys.argv) > 4 and sys.argv[3] == "MATRIX":
            fileOut = sys.argv[4]
            # print ("MODE MATRIX : " + fileOut)
            todo = {
                "Mat": set(["*M"]),
                "Col": set(["*C"]),
                "Row": set(["*R"]),
                "Cell": set(["*Cell"]),
            }
            compareTools.cmpNodes = compareTools.filteredMetric
            compareTools.cmpEdges = compareTools.filteredMetric
            for (n, s) in todo.items():
                compareTools.selectedLabelSet = s
                n1 = Lg(fileName1)
                n2 = Lg(fileName2)
                out = n1.compare(n2)
                outStream = open(fileOut + n + ".m", "w")
                writeMetrics(out, outStream)
                outStream.close()
            compareTools.selectedLabelSet = set([])
            compareTools.ignoredLabelSet = set(["*M", "*C", "*R", "*Cell"])
            n1 = Lg(fileName1)
            n2 = Lg(fileName2)
            out = n1.compare(n2)
            outStream = open(fileOut + "Symb.m", "w")
            writeMetrics(out, outStream)

        else:

            if "diff" in sys.argv:
                showMetrics = False
            elif "m" in sys.argv:
                showErrors = False
            n1 = Lg(fileName1)
            n2 = Lg(fileName2)

            if "INTER" in sys.argv:
                n1.labelMissingEdges()
                n2.labelMissingEdges()
            # print n1.csv()
            # print n2.csv()

            out = n1.compare(n2)

            if showMetrics:
                writeMetrics(out, sys.stdout)
            if showErrors:
                writeDiff(out[1], out[3], out[2], sys.stdout)


if __name__ == "__main__":

    main()

